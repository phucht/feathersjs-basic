const feathers = require("@feathersjs/feathers");
const app = feathers();

//register simple service that returns the name and some texr
app.use("todos", {
  async get(name) {
    //Return an object in form of {name, text}
    return {
      name,
      text: "You have to do ${name}"
    };
  }
});

//A func that gets and logs a todo from the service
async function getTodo(name) {
  //Get the service we registed above
  const service = app.service("todos");
  //Call the 'get' method with a name
  const todo = await service.get(name);

  //log the todo we got back
  console.log(todo);
}

getTodo("dishes");
